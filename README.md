# Endcard Checker

## Instructions

Download this code repository.

```
git clone git@gitlab.com:liammcdermott/endcard-checker.git
cd endcard-checker
```

Installation on Ubuntu 18.04 (this works on headless servers, no desktop or
monitor required):

```
sudo apt install python3-pip firefox-geckodriver
pip3 install wget selenium
python3 endcard-checker.py https://www.youtube.com/playlist?list=PLAIcZs9N4170w_uH38v0fOPjaUgCm4e_E
```