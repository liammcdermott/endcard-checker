from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import NoSuchElementException
from time import sleep
import sys
import wget
import os
import argparse
import re

endcardLeftSelector = "div.ytp-ce-element.ytp-ce-video.ytp-ce-top-left-quad"
endcardRightSelector = "div.ytp-ce-element.ytp-ce-video.ytp-ce-top-right-quad"

def IsUri(x):
    uri = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\), ]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', x)
    return bool(uri)

parser = argparse.ArgumentParser(description="Check for the existence of endcards on a Youtube video")
parser.add_argument("playlist_id", type=str,
                    help="the id of the Youtube playlist (can also be the URL of a Youtube playlist)")
args = parser.parse_args()
if IsUri(args.playlist_id):
  playlistUri = args.playlist_id
else:
  playlistUri = "https://www.youtube.com/playlist?list=" + args.playlist_id

options = Options()
errorCount = 0
options.headless = True
driver = webdriver.Firefox(options = options)

# Without uBlock Youtube ads will load instead of the endcards, makes
# testing impossible.
extensionDir = "/tmp/endcardchecker/"
uBlockFile = "ublock_origin-1.24.2-an+fx.xpi"
if not os.path.exists(extensionDir):
  os.mkdir(extensionDir)
if not os.path.exists(extensionDir + uBlockFile):
  uBlockUri = "https://addons.mozilla.org/firefox/downloads/file/3452970/" + uBlockFile
  wget.download(uBlockUri, out=extensionDir)
  print("")
driver.install_addon(extensionDir + uBlockFile)

driver.get(playlistUri)
print(driver.title)

# Repeatedly scrolling to the bottom of the page loads the entire playlist.
driver.execute_script("window.scrollTo(0, 100000)")
while driver.find_elements_by_css_selector('.paper-spinner'):
  sleep(0.5)
  driver.execute_script("window.scrollTo(0, 100000)")

getPLVids = "return [...new Set([...document.querySelectorAll('[href^=\"/watch\"][href*=\"list\"')].reduce((x,y) => { x.push(y.href); return x; }, []))]"
plVids = driver.execute_script(getPLVids)
for vid in plVids:
  driver.get(vid)
  # Give time for JS to initialise and create endcard elements.
  sleep(1)
  try:
    endcardLeft = driver.find_element_by_css_selector(endcardLeftSelector)
  except NoSuchElementException:
    print("This video lacks a left endcard: " + vid)
    errorCount += 1
  try:
    endcardRight = driver.find_element_by_css_selector(endcardRightSelector)
  except NoSuchElementException:
    print("This video lacks a right endcard: " + vid)
    errorCount += 1

driver.quit()
print("Completed with: " + str(errorCount) + " missing endcard(s).")
if errorCount == 0:
  sys.exit(0)
else:
  sys.exit(1)
